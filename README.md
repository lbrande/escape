# Escape from Nowhere

Race as far as you can with limited fuel.

Made for Ludum Dare 50 in Godot.

Playable at [https://lbrande.itch.io/escape-from-nowhere](https://lbrande.itch.io/escape-from-nowhere).

![Racing](images/racing.png)

![Game Over](images/game_over.png)
