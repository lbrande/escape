extends Node2D

const SEGMENT_SIZE := 800.0

const SEGMENTS := [
	preload("res://segments/straight.tscn"),
	preload("res://segments/around.tscn"),
	preload("res://segments/chicane_curve.tscn"),
	preload("res://segments/chicane_straight.tscn"),
	preload("res://segments/curve.tscn"),
	preload("res://segments/long.tscn"),
	preload("res://segments/snake.tscn"),
	preload("res://segments/squiggle.tscn"),
]

const EMPTY_FUEL_BOX := preload("res://fuel/empty.png")

const FUEL_BOXES := [
	preload("res://fuel/red.png"),
	preload("res://fuel/orange.png"),
	preload("res://fuel/yellow.png"),
	preload("res://fuel/green.png"),
]

var _last_segment: Segment = SEGMENTS[0].instance()
var _segments := { Vector2(0, 0): _last_segment }
var _used_segments := []
var _current_segment_position := Vector2(0, 0)
var _fuel_left := 5
var _score := 0

onready var _fuel_boxes := [
	$CanvasLayer/TextureRect,
	$CanvasLayer/TextureRect2,
	$CanvasLayer/TextureRect3,
	$CanvasLayer/TextureRect4,
	$CanvasLayer/TextureRect5
]

onready var _car: Node = $Car
onready var _title: Label = $CanvasLayer/Label
onready var _info_label: Label = $CanvasLayer/Label2
onready var _score_label: Label = $CanvasLayer/Label3
onready var _fuel_down: AudioStreamPlayer = $AudioStreamPlayer
onready var _fuel_up: AudioStreamPlayer = $AudioStreamPlayer2
onready var _crash: AudioStreamPlayer = $AudioStreamPlayer3
onready var _timer: Timer = $Timer

func _ready() -> void:
	_last_segment.connect("body_entered", self, "_on_Segment_body_entered")
	add_child(_last_segment)

	randomize()
	for _i in range(2):
		_generate_segment()


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		if !_car.game_started:
			_start_game()
		elif _car.game_over:
			self.queue_free()
			get_tree().reload_current_scene()


func _process(_delta: float) -> void:
	var new_segment_position := _to_segment_position(_car.position)
	if !_car.game_over and new_segment_position != _current_segment_position:
		var old_score = _score
		_score += _segments[_current_segment_position].value
		_score_label.text = str(_score)
		if _score / 50 > old_score / 50 and _fuel_left < 5:
			_fuel_up.play()
			_fuel_boxes[_fuel_left].texture = FUEL_BOXES[_fuel_left - 1]
			_fuel_left += 1
		_used_segments.append(_current_segment_position)
		if _used_segments.size() > 1:
			var segment_position = _used_segments.pop_front()
			remove_child(_segments[segment_position])
			_segments.erase(segment_position)
		_current_segment_position = new_segment_position
		_generate_segment()


func _on_Segment_body_entered(_body: Node) -> void:
	_game_over()


func _on_Timer_timeout() -> void:
	_fuel_left -= 1
	if _fuel_left >= 0:
		_fuel_down.play()
		_fuel_boxes[_fuel_left].texture = EMPTY_FUEL_BOX
	if _fuel_left == 0:
		_game_over()


func _start_game() -> void:
	_title.hide()
	_info_label.hide()
	_score_label.show()
	for led in _fuel_boxes:
		led.show()
	_car.game_started = true
	_timer.start()


func _game_over() -> void:
	if !_car.game_over:
		_timer.stop()
		_car.game_over = true
		_crash.play()
		_score_label.hide()
		for led in _fuel_boxes:
			led.hide()
		_title.show()
		_info_label.text = ("Game Over\n\nScore: " + str(_score) +
				"\n\nPress (A)/ENTER to continue")
		_info_label.show()


func _generate_segment() -> void:
	var type := int(rand_range(0, SEGMENTS.size()))
	var rotations := _last_segment.exit - 2
	var flipped := rand_range(0, 1) < 0.5
	var next_segment: Segment = SEGMENTS[type].instance()

	if flipped:
		if _last_segment.exit % 2 == 0:
			next_segment.scale.x = -1
		else:
			next_segment.scale.y = -1
		next_segment.exit = (next_segment.exit + next_segment.exit % 2 * 2
				+ rotations) % 4
		rotations = -rotations
	else:
		next_segment.exit = (next_segment.exit + rotations) % 4
	next_segment.rotation = rotations * PI / 2
	next_segment.connect("body_entered", self, "_on_Segment_body_entered")

	_position_relative_to(next_segment, _last_segment, _last_segment.exit)
	var segment_position = _to_segment_position(next_segment.position)
	if _segments.has(segment_position):
		remove_child(_segments[segment_position])
		_used_segments.erase(segment_position)
	_segments[segment_position] = next_segment
	add_child(next_segment)
	_last_segment = next_segment


func _position_relative_to(segment: Segment, relative_to: Segment,
		direction: int) -> void:
	match direction:
		0:
			segment.position.x = relative_to.position.x
			segment.position.y = relative_to.position.y + SEGMENT_SIZE
		1:
			segment.position.x = relative_to.position.x - SEGMENT_SIZE
			segment.position.y = relative_to.position.y
		2:
			segment.position.x = relative_to.position.x
			segment.position.y = relative_to.position.y - SEGMENT_SIZE
		3:
			segment.position.x = relative_to.position.x + SEGMENT_SIZE
			segment.position.y = relative_to.position.y
		4:
			segment.position.x = relative_to.position.x - SEGMENT_SIZE
			segment.position.y = relative_to.position.y + SEGMENT_SIZE
		5:
			segment.position.x = relative_to.position.x - SEGMENT_SIZE
			segment.position.y = relative_to.position.y - SEGMENT_SIZE
		6:
			segment.position.x = relative_to.position.x + SEGMENT_SIZE
			segment.position.y = relative_to.position.y - SEGMENT_SIZE
		7:
			segment.position.x = relative_to.position.x + SEGMENT_SIZE
			segment.position.y = relative_to.position.y + SEGMENT_SIZE


func _to_segment_position(position: Vector2) -> Vector2:
	return Vector2(int(round(position.y / SEGMENT_SIZE)),
			int(round(position.x / SEGMENT_SIZE)))
