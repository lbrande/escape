extends RigidBody2D

const GEAR_STEP := 30
const GEAR_FORCE := Vector2(0, -30)
const MOTOR_FORCE := Vector2(0, -200)
const FRICTION := Vector2(10, 0)
const MIN_TURN_SPEED := 60.0
const WHEEL_FORCE := Vector2(150, 0)
const WHEEL_FORCE_FACTOR := 400.0
const FRONT_POSITION := Vector2(0, -21)

var game_started := false
var game_over := false

onready var _engine_sound: AudioStreamPlayer = $AudioStreamPlayer
onready var _sprite: AnimatedSprite = $AnimatedSprite

func _process(_delta: float) -> void:
	if game_over:
			_engine_sound.stop()
	elif Input.is_action_pressed("accelerate"):
		if !_engine_sound.playing:
			_engine_sound.play()
	else:
		_engine_sound.stop()


func _physics_process(delta: float) -> void:
	if !game_started || game_over:
		linear_velocity = Vector2.ZERO
		angular_velocity = 0
		return

	var force := Vector2.ZERO

	var gear := int(linear_velocity.length()) / GEAR_STEP + 1
	force += ((MOTOR_FORCE +  GEAR_FORCE * gear).rotated(rotation) *
		Input.get_action_strength("accelerate"))

	var friction := (FRICTION.rotated(rotation) * delta
			* (linear_velocity.dot(Vector2(-1, 0).rotated(rotation))))
	apply_impulse(Vector2.ZERO, friction)

	if linear_velocity.length() > MIN_TURN_SPEED:
		var wheel_force := (WHEEL_FORCE.rotated(rotation)
				* sqrt(WHEEL_FORCE_FACTOR / linear_velocity.length()))
		var axis = Input.get_axis("steer_left", "steer_right")
		if axis == 0:
			axis = Input.get_axis("steer_left_gamepad", "steer_right_gamepad")
		force += wheel_force * axis
		if axis < 0:
			_sprite.animation = "turn_left"
		elif axis > 0:
			_sprite.animation = "turn_right"
		else:
			_sprite.animation = "default"
	else:
		_sprite.animation = "default"

	apply_impulse(FRONT_POSITION.rotated(rotation), force * delta)
